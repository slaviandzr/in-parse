const { Pool } = require('pg');
const parse = require('./components/parse');
const slack = require('./components/slack');
const config = require('./env/config');
const cmd = require('node-cmd');
let logger = require('nodejslogger');
logger.init({"file":"log.txt"});

let all = {
    count: 0,
    errors: 0
};

const pool = new Pool(config.db);

pool.query('SELECT DISTINCT user_id, account_id, account_id_api, url, username_api FROM users_sm_accounts WHERE type = $1 AND activated_type = $2 ORDER BY url', ['I', 'A'])
    .then((res) => {
        let accounts = res.rows;

        pool.query('SELECT media_id_api FROM users_sm_media WHERE is_trash != $1 ORDER BY media_id', [true])
            .then((res) => {
                let mediaNodes = res.rows;

                let arr = [];

                let mediaNodesLength = res.rows.length;

                for (let j = 0; j < mediaNodesLength; j++) {
                    arr.push(res.rows[j].media_id_api);
                }

                let dataLength = accounts.length;
                all.count = dataLength;

                console.log('Parser started at ' + new Date() + '. Please see log.txt after process finished for details.' + '\r\n' + 'Total active accounts: ' + dataLength + '. Working........');

                for (let i = 0 ; i < dataLength; i++) {
                        setTimeout(function()
                        {
                            console.log('Remaining: ' + all.count + '(' + accounts[i].username_api + ')');
                            all.count--;
                            parse.getInstaData(accounts[i], arr);
                            if (all.count < 1) {
                                console.log('Exiting by timeout 10m');
                                setTimeout(function () {
                                    slack.send('Instagram parser finished, total: ' + dataLength + ' processed. Errors: ' + all.errors + '. Please see log.')
                                }, 400000);
                                setTimeout(function () {
                                    cmd.run('sudo killall phantomjs');
                                    process.exit();
                                }, 600000);
                            }
                        }, ((Math.floor(Math.random() * config.delay.max) + config.delay.min) * i), i);
                }

                // let x = 0;
                //
                // let interval = setInterval(function () {
                //    if (x < 4) {
                //        parse.getInstaData(accounts[x], arr);
                //        x ++;
                //    } else {
                //        return clearInterval(interval);
                //    }
                // }, 7000);

                // for (let i = 0; i < dataLength; i++) {
                //     (function(ind) {
                //         setTimeout(function()
                //         {
                //             parse.getInstaData(accounts[i], arr);
                //         }, ((Math.floor(Math.random() * config.delay.max) + config.delay.min) * ind));
                //     })(i);
                // }
            })
            .catch(err => {
                // console.error('Error executing query', err.stack);
                all.errors++;
                logger.error('Error executing query', err.stack);
            })
    })
    .catch(err => {
        // console.error('Error executing query', err.stack);
        all.errors++;
        logger.error('Error executing query', err.stack);

    });