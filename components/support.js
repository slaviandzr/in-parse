const config = require('../env/config');

function getUser(res) {
    if (!res.entry_data.ProfilePage) {
        return false;
    }
    return res.entry_data.ProfilePage[0].graphql.user;
}

function getImageStandartSrcApi(post) {
    if (post.node.thumbnail_resources.length > 0) {
        return post.node.thumbnail_resources[4].src;
    } else return false;
}

function getCaptionTextApi(post) {
    if (post.node.edge_media_to_caption.edges.length > 0) {
        return post.node.edge_media_to_caption.edges[0].node.text;
    } else return false;
}

function getMediaIdApi(post, account) {
    return post.node.id + '_'+ account.account_id_api;
}

function getLinkApi(post) {
    return config.url + config.url_path + post.node.shortcode + '/';
}

function getLikesCountApi(post) {
    return post.node.edge_liked_by.count;
}

function getCommentsCountApi(post) {
    return post.node.edge_media_to_comment.count;
}

function getCaptionCreatedTimeApi(post) {
    return post.node.taken_at_timestamp;
}

function getPosts(node) {
    return node.user_data.edge_owner_to_timeline_media.edges;
}

function isVideo(post) {
    return post.is_video === true;
}

function getUrl() {
    return config.url;
}


module.exports = {
    getUser: getUser,
    getImageStandartSrcApi: getImageStandartSrcApi,
    getCaptionTextApi: getCaptionTextApi,
    getMediaIdApi: getMediaIdApi,
    getLinkApi: getLinkApi,
    getLikesCountApi: getLikesCountApi,
    getCommentsCountApi: getCommentsCountApi,
    getCaptionCreatedTimeApi: getCaptionCreatedTimeApi,
    getPosts: getPosts,
    isVideo: isVideo,
    getUrl: getUrl
};