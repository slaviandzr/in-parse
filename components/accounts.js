const { Pool } = require('pg');
const config = require('../env/config');

const pool = new Pool(config.db);

pool.query('SELECT account_id_api, url FROM users_sm_accounts WHERE type = $1', ['I'])
    .then((res) => {
        console.log(res.rows);
        pool.query('SELECT media_id_api FROM users_sm_media WHERE is_trash != $1 AND status != $2', [true, 'N'])
            .then((res) => console.log(res.rows))
            .catch(err => console.error('Error executing query', err.stack))
    })
    .catch(err => console.error('Error executing query', err.stack));