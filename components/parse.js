const phantom = require('phantom');
const support = require('./support');
const cp = require('child_process');
const config = require('../env/config');
let logger = require('nodejslogger');
logger.init({"file": "log.txt"});

let isJSON = require('is-valid-json');

const {Pool} = require('pg');

const pool = new Pool(config.db);

let rootPath = require('app-root-path');
let _ph, _page;


function parseContent(content) {

    let data = content.substring(content.indexOf('window._sharedData = ') + 21);

    let jsonData = data.substring(0, data.indexOf(';</script>'));

    if (isJSON(jsonData)) {
        let result = JSON.parse(jsonData);

            if (result) {

                let user = support.getUser(result);

                if (user) {
                    let nodes = user.edge_owner_to_timeline_media.edges;
                    return {user_id: user.id, user_data: user};
                }
            } else {
                logger.error('Broken JSON data detected');
            }
    }
    else {
        logger.error('Broken JSON detected, may be locked or no posts');
    }

}


function getInstaData(account, arr) {
    phantom
        .create()
        .then(ph => {
            _ph = ph;
            return _ph.createPage();
        })
        .then(page => {
            _page = page;
            // return _page.open(support.getUrl() + account.url);
            return _page.open(support.getUrl() + account.username_api);
        })
        .then(status => {
            return _page.property('content');
        })
        .then(content => {
            return parseContent(content);
        })
        .then((node) => {
                if (node) {

                    if (JSON.stringify(node.user_data)) {

                        if (!JSON.stringify(node.user_data).includes('mylook.blog/' + account.username_api)
                            && !JSON.stringify(node.user_data).includes('mylook.blog/' + node.user_data.username)
                            && !JSON.stringify(node.user_data).includes('mylook.blog/' + account.url)) {
                            // console.log('Empty mylook link, user name is : ' + node.user_data.username);
                            logger.info('Empty mylook link, user name is : ' + node.user_data.username);
                        } else {

                            let posts = support.getPosts(node);

                            let postsLength = posts.length;

                            for (let k = 0; k < postsLength; k++) {

                                if (support.isVideo(posts[k].node) || (arr.includes('' + posts[k].node.id + '_' + node.user_id + '') || arr.includes(posts[k].node.id + '_' + node.user_id + '_' + node.user_id))) {
                                    // console.log('Post ' + posts[k].node.id + '_' + node.user_id + ' already added or its a video: isVideo = ' + support.isVideo(posts[k].node));
                                    logger.info('Post ' + posts[k].node.id + '_' + node.user_id + ' already added');
                                } else {

                                    pool.query('INSERT into users_sm_media ' +
                                        '(media_id_api, ' +
                                        'user_id, ' +
                                        'account_id, ' +
                                        'account_id_api, ' +
                                        'link_api, ' +
                                        'image_standart_src_api, ' +
                                        'created_time_api, ' +
                                        'updated_time, ' +
                                        'tags_api, ' +
                                        'likes_count_api, ' +
                                        'comments_count_api, ' +
                                        'caption_text_api, ' +
                                        'caption_created_time_api, ' +
                                        'status, ' +
                                        'status_smm) ' +
                                        'VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15) RETURNING media_id',
                                        [
                                            support.getMediaIdApi(posts[k], account),
                                            account.user_id,
                                            account.account_id,
                                            account.account_id_api,
                                            support.getLinkApi(posts[k]),
                                            support.getImageStandartSrcApi(posts[k]),
                                            Math.floor(Date.now() / 1000),
                                            Math.floor(Date.now() / 1000),
                                            {},
                                            support.getLikesCountApi(posts[k]),
                                            support.getCommentsCountApi(posts[k]),
                                            support.getCaptionTextApi(posts[k]),
                                            support.getCaptionCreatedTimeApi(posts[k]),
                                            'D', // automoderate
                                            'A'
                                        ],
                                        function (err, result) {
                                            if (err) {
                                                logger.error(err);
                                            } else {
                                                let child = cp.fork(rootPath + '/components/uploader.js', [support.getImageStandartSrcApi(posts[k]), support.getMediaIdApi(posts[k], account)]);
                                                // console.log('row inserted with id: ' + result.rows[0].media_id);
                                                // console.log('Post for '
                                                //     + node.user_data.username
                                                //     + ', link: '
                                                //     + support.getLinkApi(posts[k])
                                                //     + ' saved.'
                                                //     + ' media_id_api: ' + support.getMediaIdApi(posts[k], account));
                                                logger.info('Post for '
                                                    + node.user_data.username
                                                    + ', link: '
                                                    + support.getLinkApi(posts[k])
                                                    + ' saved.'
                                                    + ' media_id_api: ' + support.getMediaIdApi(posts[k], account));

                                            }
                                        });
                                }
                            }
                        }
                    }

                }
                _page.close();
            }
        )
        .then(() => {
            _ph.exit();
        })
        .catch(e => {
            logger.error(e.stack);
            // console.log(e);
        });
}

module.exports = {
    getInstaData: getInstaData
};