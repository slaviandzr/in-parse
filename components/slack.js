const { IncomingWebhook } = require('@slack/client');
const workers_url = '********************';
let logger = require('nodejslogger');
logger.init({"file": "log.txt"});

function sendSlack(text) {

    let workershook = new IncomingWebhook(workers_url);

    workershook.send(text, function(err, res) {
        if (err) {
            logger.error('Error: ', err);
        } else {
            logger.info('Message sent to slack channel "workers_alert": ', res);
        }
    });
}

module.exports = {
  send: sendSlack
};